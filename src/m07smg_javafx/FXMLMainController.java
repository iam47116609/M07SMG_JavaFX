/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m07smg_javafx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javax.swing.event.DocumentEvent;
import static m07smg_javafx.M07SMG_JavaFX.escena;
import static m07smg_javafx.M07SMG_JavaFX.window;

/**
 *
 * @author iam76654250
 */
public class FXMLMainController implements Initializable {

    @FXML
    Label tempHome;

    @FXML
    Label tempMin;
    @FXML
    Label tempMax;

    @FXML
    Label tempParcela;

    @FXML
    ProgressBar tempMainPB;

    @FXML
    ProgressBar tempParcelaPB;

    @FXML
    Label tempModParcela;

    @FXML
    Slider tempSliderParcela;

    @FXML
    private void parcelaSwitch1(ActionEvent event) {
        try {
            escena = 1;
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLParcela.fxml")));
            window.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void parcelaSwitch2(ActionEvent event) {
        try {
            escena = 1;
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLParcela.fxml")));
            window.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void parcelaSwitch3(ActionEvent event) {
        try {
            escena = 1;
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLParcela.fxml")));
            window.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void parcelaSwitch4(ActionEvent event) {
        try {
            escena = 1;
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLParcela.fxml")));
            window.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void edificioSwitch(ActionEvent event) {
        try {
            escena = 5;
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLEdificio.fxml")));
            window.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void plantaSwitch1(ActionEvent event) {
        try {
            escena = 5;
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLEdificio.fxml")));
            window.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void plantaSwitch2(ActionEvent event) {
        try {
            escena = 5;
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLEdificio2.fxml")));
            window.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void homeSwitch(ActionEvent event) {
        try {
            escena = 0;
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLMain.fxml")));
            window.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (escena == 0 || escena == 5) {
            int tmp = (int) Math.floor(Math.random() * 32 - 5);
            tempHome.setText(String.valueOf(tmp));
            if (escena == 5) {
                tempMax.setText(String.valueOf(tmp + 1));
                tempMin.setText(String.valueOf(tmp - 2));
            }
            if (escena == 0) {
                double percent = (tmp + 5) * 1 / 37d;
                tempMainPB.setProgress(percent);
            }
        } else if (escena == 1) {
            int tmp = (int) Math.floor(Math.random() * 50 - 20);
            double percent = (tmp + 20) * 1 / 70d;
            tempParcela.setText(String.valueOf(tmp));

            tempParcelaPB.setProgress(percent);
            
            tempModParcela.setText(String.valueOf(0));

            tempSliderParcela.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                tempModParcela.setText(String.valueOf((int)newValue.doubleValue()/2));
            });

        }
    }

}
